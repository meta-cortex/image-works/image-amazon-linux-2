packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "ami_prefix" {
  type    = string
  default = "ami-metacortex-amazon-linux-2"
}

variable "region" {
    type = string
    description   = "The AWS region where the image will be build."
    default = "us-east-1"
    
    validation {
        condition     = length(var.region) > 0
        error_message = "The AWS_DEFAULT_REGION environment variable must be set."
    }
}


data "amazon-ami" "amzn2" {
  filters = {
    virtualization-type = "hvm"
    name                = "amzn2-ami-hvm-2.0.*ebs"
    root-device-type    = "ebs"
  }
  owners      = ["amazon"]
  most_recent = true
  region      = var.region
}


locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "")
}

source "amazon-ebs" "amazon-linux-2" {
  ami_name      = "${var.ami_prefix}-${local.timestamp}"
  instance_type = "t2.micro"
  region        = "us-east-1"
  source_ami    = data.amazon-ami.amzn2.id
  ssh_username  = "ec2-user"
}

build {
  name = "amazon-linux-2"
  sources = [
    "source.amazon-ebs.amazon-linux-2"
  ]

  provisioner "shell" {
    inline = ["echo This provisioner runs last"]
  }

}